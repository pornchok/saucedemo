*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0009: User can login with a problem user account
    [Tags]      login       tc_0009
    login_feature.Open web swaglabs and login       ${tc_0009['username']}      ${tc_0009['password']}
    inventory_page.Verify header page display the word "product"