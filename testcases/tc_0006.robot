*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0006: User can't login with wrong password
    [Tags]      login       tc_0006
    login_feature.Open web swaglabs and login       ${tc_0006['username']}      ${tc_0006['password']}
    login_feature.Verify that login fails with username and password are not matching