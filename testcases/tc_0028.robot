*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0028: User can't checkout without entering first name, last name, zip code
    [Tags]      checkout        tc_0028
    login_feature.Open web swaglabs and login                                   ${tc_0028['username']}      ${tc_0028['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart        ${tc_0028['product_name']}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_page.Click continue button
    checkout_feature.Verify that checkout fails with firstname required
    