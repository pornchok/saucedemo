*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0016: Verify that user can click the "Add to cart" button and cart icon should display the number of items added to the cart
    [Tags]      inventory       tc_0016
    login_feature.Open web swaglabs and login                               ${tc_0016['username']}      ${tc_0016['password']}
    inventory_page.Click add to cart button                                 ${tc_0016['product_name_1']}
    inventory_page.Click add to cart button                                 ${tc_0016['product_name_2']}
    inventory_page.Click add to cart button                                 ${tc_0016['product_name_3']}
    inventory_page.Verify that number on cart icon display correctly        ${tc_0016['no_of_item']}