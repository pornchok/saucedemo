*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0035: Overview page display information correctly and user can click "Finish" button
    [Tags]      overview        tc_0035
    login_feature.Open web swaglabs and login                                       ${tc_0035['username']}      ${tc_0035['password']}
    ${product_detail_1}=        inventory_feature.Get detail and add to cart        ${tc_0035['product_name_1']}
    ${product_detail_2}=        inventory_feature.Get detail and add to cart        ${tc_0035['product_name_2']}
    ${product_detail_list}=     BuiltIn.Create list                                 ${product_detail_1}         ${product_detail_2}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_feature.Continue checkout                                              ${tc_0035['first_name']}    ${tc_0035['last_name']}    ${tc_0035['postal_code']}

    overview_feature.Verify that all product detail display correctly               ${product_detail_list}
    overview_page.Verify that payment information display correctly                 ${tc_0035['payment_information']}
    overview_page.Verify that shipping information display correctly                ${tc_0035['shipping_information']}

    ${item_total}=      overview_page.Calulate item total                           ${product_detail_list}
    overview_page.Verify that item total display correctly                          ${item_total}
    ${tax}=     overview_page.Get calculate tax                                     ${tc_0035['tax']}       ${item_total}
    
    overview_page.Verify that tax display correctly                                 ${tax}
    ${total}=       overview_page.Get calculate total                               ${tax}      ${item_total}
    overview_page.Verify that total display correctly                               ${total}

    overview_page.Click finish button
    checkout_complete_page.Verify that page display text "Thank you for your order"