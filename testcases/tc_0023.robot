*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0023: Cart page display product based on the items that have been added to the cart
    [Tags]      cart        tc_0023
    login_feature.Open web swaglabs and login                               ${tc_0023['username']}      ${tc_0023['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart    ${tc_0023['product_name']}
    header_container.Click cart icon
    cart_page.Verify that product detail on cart display correctly          ${product_detail}