*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0033: User can't input last name field. If login with problem user account
    [Tags]      checkout        tc_0033
    login_feature.Open web swaglabs and login                                   ${tc_0033['username']}      ${tc_0033['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart        ${tc_0033['product_name']}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_page.Input first name                                              ${tc_0033['first_name']}
    checkout_page.Input last name                                               ${tc_0033['last_name']}
    ${last_name}=       checkout_page.Get last name
    BuiltIn.Should be empty                                                     ${last_name}