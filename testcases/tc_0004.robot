*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0004: User can't login without entering password
    [Tags]      login       tc_0004
    common.Open chrome browser      ${URL['SAUCEDEMO']}
    login_page.Input username       ${tc_0004['username']}
    login_page.Click login button
    login_feature.Verify that login fails with password required