*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0007: User can't log in with not exist user
    [Tags]      login       tc_0007
    login_feature.Open web swaglabs and login       ${tc_0007['username']}      ${tc_0007['password']}
    login_feature.Verify that login fails with username and password are not matching