*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0014: Sort function from "Price (high to low)" should sorted correctly
    [Tags]      inventory       tc_0014
    login_feature.Open web swaglabs and login                               ${tc_0014['username']}      ${tc_0014['password']}
    ${product_list_api}=    product_api.Get all product sorted by price     ${tc_0014['order_by']}
    inventory_feature.Filter with price high to low
    ${product_list_ui}=     inventory_page.Get product list
    inventory_page.Verify that product display correctly                    ${product_list_ui}     ${product_list_api}