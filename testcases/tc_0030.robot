*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0030: User can't checkout without entering last name
    [Tags]      checkout        tc_0030
    login_feature.Open web swaglabs and login                                   ${tc_0030['username']}      ${tc_0030['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart        ${tc_0030['product_name']}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_page.Input first name                                              ${tc_0030['first_name']}
    checkout_page.Input postal code                                             ${tc_0030['postal_code']}
    checkout_page.Click continue button
    checkout_feature.Verify that checkout fails with lastname required