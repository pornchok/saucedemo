*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0011: Inventory page should display all product
    [Tags]      inventory       tc_0011
    login_feature.Open web swaglabs and login                   ${tc_0011['username']}      ${tc_0011['password']}
    ${product_list_api}=    product_api.Get all product
    ${product_list_ui}=     inventory_page.Get product list
    inventory_page.Verify that product display correctly        ${product_list_ui}     ${product_list_api}
