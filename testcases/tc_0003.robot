*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0003: User can't login without entering username
    [Tags]      login       tc_0003
    common.Open chrome browser      ${URL['SAUCEDEMO']}
    login_page.Input password       ${tc_0003['password']}
    login_page.Click login button
    login_feature.Verify that login fails with username required