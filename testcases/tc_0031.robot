*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0031: User can't checkout without entering postal code
    [Tags]      checkout        tc_0031
    login_feature.Open web swaglabs and login                                   ${tc_0031['username']}      ${tc_0031['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart        ${tc_0031['product_name']}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_page.Input first name                                              ${tc_0031['first_name']}
    checkout_page.Input last name                                               ${tc_0031['last_name']}
    checkout_page.Click continue button
    checkout_feature.Verify that checkout fails with postal code required