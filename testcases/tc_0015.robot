*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0015: Sort function from "Price (low to high)" should sorted correctly
    [Tags]      inventory       tc_0015
    login_feature.Open web swaglabs and login                               ${tc_0015['username']}      ${tc_0015['password']}
    ${product_list_api}=    product_api.Get all product sorted by price     ${tc_0015['order_by']}
    inventory_feature.Filter with price low to high
    ${product_list_ui}=     inventory_page.Get product list
    inventory_page.Verify that product display correctly                    ${product_list_ui}     ${product_list_api}