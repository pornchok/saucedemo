*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0029: User can't checkout without entering first name
    [Tags]      checkout        tc_0029
    login_feature.Open web swaglabs and login                                   ${tc_0029['username']}      ${tc_0029['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart        ${tc_0029['product_name']}
    header_container.Click cart icon
    cart_page.Click checkout button
    checkout_page.Input last name                                               ${tc_0029['last_name']}
    checkout_page.Input postal code                                             ${tc_0029['postal_code']}
    checkout_page.Click continue button
    checkout_feature.Verify that checkout fails with firstname required