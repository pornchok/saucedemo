*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0024: Verify that user can click "Remove" button
    [Tags]      cart        tc_0024
    login_feature.Open web swaglabs and login                               ${tc_0024['username']}      ${tc_0024['password']}
    ${product_detail}=      inventory_feature.Get detail and add to cart    ${tc_0024['product_name']}
    header_container.Click cart icon
    cart_page.Verify that product detail on cart display correctly          ${product_detail}
    cart_feature.Remove product in cart                                     ${tc_0024['product_name']}