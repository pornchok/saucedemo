*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0008: User can't log in with locked user account
    [Tags]      login       tc_0008
    login_feature.Open web swaglabs and login       ${tc_0008['username']}      ${tc_0008['password']}
    login_feature.Verify that login fails with user locked