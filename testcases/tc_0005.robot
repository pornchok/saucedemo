*** Settings ***
Resource        ${CURDIR}/../imports/imports.resource
Test Teardown   common.Default test teardown

*** Test Cases ***
TC_0005: User can't login without entering username and password
    [Tags]      login       tc_0005
    common.Open chrome browser      ${URL['SAUCEDEMO']}
    login_page.Click login button
    login_feature.Verify that login fails with username required